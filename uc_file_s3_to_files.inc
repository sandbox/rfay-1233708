<?php

/**
 * @file
 *   Commerce File License migration from uc_file_s3.
 */

/**
 * TIP: Files can be migrated directly by themselves, by using the MigrateDestinationFile
 * class. This will copy the files themselves from the source, and set up the
 * Drupal file tables appropriately.
 */
class CommerceMigrateUCFileS3ToFilesMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Bring in files from uc_file_s3');
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
          'fid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'uc_files_s3 key',
          ),
      ),
        MigrateDestinationFile::getKeySchema()
    );
    $connection = commerce_migrate_ubercart_get_source_connection();
    $query = $connection->select('uc_files_s3', 'ufs')
             ->fields('ufs', array('fid', 'filename'));
    $query->condition('filename', 'logs/%', 'NOT LIKE')
    ->condition('filename', 'log/%', 'NOT LIKE')
    ->condition('filename', 'cflog/%', 'NOT LIKE')
    ->condition('filename', 'stats/%', 'NOT LIKE');
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    // TIP: Set copy_file to copy the file from its source (which could be a
    // remote server, i.e. the uri is of the form http://example.com/images/foo.jpg).
    $this->destination = new MigrateDestinationFile(array('preserve_files' => TRUE));

    // Just map the incoming URL to the destination's 'uri'
    $this->addFieldMapping('uri', 'uri');
    $this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
      'filemime', 'timestamp'));
  }

  public function prepareRow($row) {
    $row->uri = 's3://' . $row->filename;
  }
}
