<?php

class MigrateCommerceFileFieldHandler extends MigrateFileFieldHandler {
  public function __construct() {
    $this->registerTypes(array('commerce_file', 'commerce_file_license_file'));
  }

  /**
   * Prepare file data for saving as a Field API file field.
   *
   * @return array
   *  Field API array suitable for inserting in the destination object.
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }
    $migration = Migration::currentMigration();
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    $return = array();
    foreach ($values as $delta => $value) {
      // Empty stuff is skipped
      if ($value) {
        $properties = drupal_json_decode($value);
        $file_array = $this->buildFileArray($entity, $field_info, $instance, $migration, $arguments, $value);
        if (!empty($file_array)) {
          $file_array['data'] = serialize(array(
            'download_limit' => $properties['download_limit'],
            'address_limit' => $properties['address_limit'],
            'duration' => $properties['duration'],
          ));
          $return[$language][$delta] = $file_array;
          if (!empty($arguments['preserve_files'])) {
            file_usage_add((object)$file_array, 'migrate', 'file', $file_array['fid']);
          }
        }
      }
    }
    return $return;
  }

  /**
   * Parses file information to create an appropriate data array.
   *
   * @param $entity
   * @param array $field_info
   * @param array $instance
   * @param $migration
   * @param $arguments
   *   Supported arguments are
   *   - 'source_path': A path to be prepended to the path of the source item.
   *   - 'skip_source_file_check': Don't bother to see if the source file is
   *     there.
   * @param $value
   *   A json-encoded array of properties:
   *   - path
   *   - alt
   *   - title
   *   - description
   *   - display
   * @return array
   */
  protected function buildFileArray($entity, array $field_info, array $instance, $migration, $arguments, $value) {

    // Is the value a straight path, or JSON containing a set of properties?
    if ($value{0} == '{') {
      $properties = drupal_json_decode($value);
      $path = $properties['path']; // Should be URI.
    }
    else {
      $path = $value;
    }

    if (!empty($arguments['source_path'])) {
      $full_path = rtrim($arguments['source_path'], "/\\") .
          '/' . ltrim($path, "/\\");

    }
    else {
      $full_path = $path;
    }

    // TODO: belongs in prepare or validate?
    // Check that source exists. If not, mark the entity as 'needs_update' and bail.
    // Sometimes the source file arrives later, when rsync is slower than DB.
    if (empty($arguments['skip_source_file_check']) && !is_file($full_path)) {
      // is_file() won't handle URLs, check to see if we have a remote file
      // (only relevant in the file_copy case)
      $migration->saveMessage(t('Source file does not exist: !path',
      array('!path' => $full_path)), Migration::MESSAGE_WARNING);
      $migration->needsUpdate = TRUE;
      return;
    }

    $real_destination_file = drupal_realpath($full_path);
    $source = (object) array(
      'uri' => $full_path,
      'uid' => isset($entity->uid) ? $entity->uid : 0,
      'filename' => basename($full_path),
      'filemime' => file_get_mimetype($full_path),
      'timestamp' => REQUEST_TIME,
    );

    migrate_instrument_start('MigrateFileFieldHandler file_function');

    // We are just updating pointers to an existing remote file.
    migrate_instrument_start('file_link: file_exists');
    if (empty($arguments['skip_source_file_check']) && !file_exists($full_path)) {
      migrate_instrument_stop('file_link: file_exists');
      $message = t('File does not exist: !path', array('!path' => $full_path));
      if ($arguments['throw_error']) {
        throw new MigrateException($message, MigrationBase::MESSAGE_ERROR);
      }
      else {
        $migration->saveMessage($message, MigrationBase::MESSAGE_INFORMATIONAL);
      }
      $message_saved = TRUE;
      // TODO     $migration->needsUpdate = TRUE;
      continue;
    }
    migrate_instrument_stop('file_link: file_exists');

    // Files table entry exists? Use that...
    migrate_instrument_start('file_link: select existing');
    // Note that indexing files.filepath can be very helpful.
    // TODO: Might be better way that straight query in D7?
    $file = db_select('file_managed', 'f')
    ->fields('f')
    ->condition('uri', $full_path)
    ->execute()
    ->fetchObject();
    migrate_instrument_stop('file_link: select existing');
    if (!$file) {
      $file = new stdClass;
    }
    migrate_instrument_start('file_link: create file record');
    $file->uri = $full_path;
    $file->filename = basename($full_path);
    $file->uid = isset($entity->uid) ? $entity->uid : 0;
    $file->status |= FILE_STATUS_PERMANENT;
    $file->filemime = file_get_mimetype($file->filename);

    $file = file_save($file);
    // Since we're really just adding a reference to a file that already
    // exists, it's important to make sure it doesn't get deleted.
    // file_usage_add($file, 'AmazonS3', 'preexisting', $file->fid);
    migrate_instrument_stop('file_link: create file record');

    migrate_instrument_stop('MigrateFileFieldHandler file_function');

    // Build up a return object.
    $object_field['fid'] = $file->fid;

    return $object_field;
  }
}


class CommerceMigrateCommerceLicenseFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    $this->registerTypes(array('commerce_file_license'));
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }

    $migration = Migration::currentMigration();
    $destination = $migration->getDestination();
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);


    // Setup the standard Field API array for saving.
    $delta = 0;
    foreach ($values as $value) {
      $value = drupal_json_decode($value);
      $item = array();
      $item['fid'] = $value['fid'];
      $item['data'] = $value['data'];
      $return[$language][$delta] = $item;
      $delta++;
    }

    return isset($return) ? $return : NULL;
  }
}
