<?php

/**
 * @file
 *   Commerce File License migration.
 *   Customized to bring in licenses from uc_file_s3
 */

class CommerceMigrateUbercartCommerceFileLicenseMigration extends Migration {

  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();
    $this->description = t('Import licenses from uc_file_s3.');

    // This doesn't need to be done if we have the same users on this system.
    $this->dependencies = array('CommerceMigrateUbercartUser');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
          'fuid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'uc_file_s3_users key',
          ),
      ),
      MigrateDestinationEntityAPI::getKeySchema('commerce_file_license')
    );
    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_ubercart_get_source_connection();
    $query = $connection->select('uc_file_s3_users', 'ufsu');
    $query->innerJoin('uc_files_s3', 'ufs', 'ufsu.fid = ufs.fid');

    $query->fields('ufsu', array('fuid', 'uid', 'granted', 'expiration', 'accessed', 'addresses', 'download_limit', 'address_limit'));
    $query->fields('ufs', array('bid', 'filename'));

    // We will have to map uids in this migration, so users must exist.
    // Licenses don't make any sense without users. It's assumed here
    // that uids in the new system are identical to uids in the old, as
    // with an upgrade. If that's not the case, a dependent migration
    // of users must be performed, and we'll map the uid using that.
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    $this->destination = new MigrateDestinationEntityAPI('commerce_file_license', 'commerce_file_license');

    // Properties
    $this->addFieldMapping('fid', 'fid');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('granted', 'granted');
    $this->addFieldMapping('status', 'status');
    // If the users on this system have been brought in using a migration, we'll
    // have to access it.
    $this->addFieldMapping('uid', 'uid')->sourceMigration('CommerceMigrateUbercartUser')->defaultValue(1); // Default to user 1.
    // Fields
    $this->addFieldMapping('commerce_file_license_line_items', 'empty_line_items');
    $this->addFieldMapping('commerce_file_license_file', 'commerce_file_license_fid')
      ->arguments(array(
        'skip_source_file_check' => TRUE,
        'preserve_files' => TRUE,
        'file_function' => 'file_link',
      ));
  }

  public function prepareRow($row) {
    // Defaulting these items as we don't seem to have a good source for them.
    $row->status = 'active';
    $row->empty_line_items = array();
    $row->duration = 0;

    $path = 's3://' . $row->filename;
    $value = array(
      'path' => $path,
      'download_limit' => $row->download_limit,
      'address_limit' => $row->address_limit,
      'duration' => 0,  // We don't seem to have a way to get this in source.
    );
    $row->commerce_file_license_fid = drupal_json_encode($value);
  }
}